#!/bin/bash

git checkout $(git rev-list --topo-order $(git show-ref -s --head | head -1)..$1 | tail -1)
